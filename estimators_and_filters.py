from sklearn import svm, linear_model
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn.feature_selection import SelectKBest, chi2, VarianceThreshold, mutual_info_classif, f_classif, \
    SelectFromModel
from sklearn.neighbors import KNeighborsClassifier

estimators = [
    {
        'name': 'knn',
        'object': KNeighborsClassifier(n_jobs=-1),
        'tuning_params': {
            'n_neighbors': [1, 3, 5, 10],
        }
    },
    {
        'name': 'svm',
        'object': svm.SVC(),
        'tuning_params': {
            'C': [10**6]
        }
    },
    {
        'name': 'log_reg',
        'object': linear_model.LogisticRegression(random_state=1337, n_jobs=-1),
        'tuning_params': {
            'C': [10**6]
        }
    },
    {
        'name': 'rf',
        'object': RandomForestClassifier(random_state=1337),
        'tuning_params': {
            'n_estimators': [10, 50, 100, 1000],
            'max_features': [5, 10, 15, 20]
        }
    },
    {
        'name': 'l1_log_reg',
        'object': linear_model.LogisticRegression(penalty='l1', random_state=1337, n_jobs=-1),
        'tuning_params': {
            'C': [0.1, 0.5, 1, 2],
        }
    }
]

tuning_k = [3, 6, 10, 15, 20]
fixed_k = 10

# filters: no filter, filter fixed parameter, filter with hyperparameters
filters = [
    {
      'name': 'chi',
      'object': SelectKBest(chi2, k=fixed_k),
    },
    {
        'name': 'chi',
        'object': SelectKBest(chi2),
        'tuning_params': {
            'k': tuning_k
        }
    },
    {
        'name': 'var',
        'object': VarianceThreshold(threshold=0.3)
    },
    {
        'name': 'var',
        'object': VarianceThreshold(),
        'tuning_params': {
            'threshold': [0.2, 0.4, 0.6, 1, 2]
        }
    },
    {
        'name': 'm_inf',
        'object': SelectKBest(mutual_info_classif, k=fixed_k)
    },
    {
        'name': 'm_inf',
        'object': SelectKBest(mutual_info_classif),
        'tuning_params': {
            'k': tuning_k
        }
    },
    {
        'name': 'f_class',
        'object': SelectKBest(f_classif, k=fixed_k)
    },
    {
        'name': 'f_class',
        'object': SelectKBest(f_classif),
        'tuning_params': {
            'k': tuning_k
        }
    },
    {
        'name': 'tree_based',
        'object': SelectFromModel(ExtraTreesClassifier(n_estimators=10), prefit=False, max_features=fixed_k)
    },
    {
        'name': 'tree_based',
        'object': SelectFromModel(ExtraTreesClassifier(), prefit=False),
        'tuning_params': {
            'max_features': tuning_k,
            'estimator__n_estimators': [5, 10, 50]
        }
    }
]
